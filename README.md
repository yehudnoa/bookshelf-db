# BookShelf DB

BIE-PJV final Project
This program lets you access a bookshelf database were you can see all the books, add a book, search for a book, delete a book, and access each books reviews and add reviews to books.

To run this project you need a database in H2 with; 
Driver Class: 'org.h2.Driver', 
JDBC URL: 'jdbc:h2:~/BookshelfDB', 
User Name: 'sa', 
Password: -EMPTY-

In the database commit :
CREATE TABLE BOOK(ID INT AUTO_INCREMENT PRIMARY KEY, NAME VARCHAR(255), AUTHOR VARCHAR(255), GENRA VARCHAR(255), YEAR INT);
CREATE TABLE REVIEW(REVIEW_ID INT AUTO_INCREMENT PRIMARY KEY, WRITTEN VARCHAR(255), FOREIGN KEY(BOOK_ID) REFERENCES BOOK(ID));