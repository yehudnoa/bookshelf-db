package fit.biepjv.bookself.model;

public class Book {
    private final int id, year;
    private final String name, author, genra;

    Book(int id, String name, String author, String genra, int year){
        this.id = id;
        this.name = name;
        this.author = author;
        this.genra = genra;
        this.year = year;
    }

    public int getId(){
        return id;
    }

    public int getYear() {
        return year;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public String getGenra() {
        return genra;
    }
    @Override
    public String toString(){
        return "Book{" + "id= " + id + ", book name='" + name +
                "', author= " + author + ", genra= " + genra +
                ", year of publication= '" + year + "}\n";
    }
}
