package fit.biepjv.bookself.model;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class BookDAO {

    public static BookDAO inst = new BookDAO();

    private Connection connection;

    BookDAO(){
        try {
            Class.forName("org.h2.Driver");
            System.out.println("Connecting to database...");
            connection = DriverManager.getConnection("jdbc:h2:~/BookshelfDB","sa", "");
            System.out.println("Connected succesfully to database");
        } catch (SQLException ex){
            throw new RuntimeException(ex);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public int add(String name, String author, String genra, int date) throws SQLException {
        try {
            PreparedStatement ps = connection.prepareStatement("insert into book(name, author, genra, year) values(?,?,?,?)");
            ps.setString(1, name);
            ps.setString(2, author);
            ps.setString(3, genra);
            ps.setInt(4, date);
            return ps.executeUpdate();
        } catch (SQLException ex){
            throw new RuntimeException(ex);
        }

    }

    public int addReview(int book_id, String review){
        try {
            PreparedStatement ps = connection.prepareStatement("insert into review(book_id, written) values(?,?)");
            ps.setInt(1, book_id);
            ps.setString(2, review);
            return ps.executeUpdate();
        } catch (SQLException ex){
            throw new RuntimeException(ex);
        }
    }

    public void delete(int id) {
        try {
            deleteReview(id);
            PreparedStatement ps = connection.prepareStatement("delete from book where id = ?");
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    public void deleteReview(int book_id) {
        try {
            PreparedStatement ps = connection.prepareStatement("delete from review where book_id = ?");
            ps.setInt(1, book_id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    public List<Review> returnReview(int book_id){
        try{
            String query = "SELECT * FROM review WHERE book_id = ?";
            PreparedStatement s = connection.prepareStatement(query);
            s.setInt(1, book_id);
            ResultSet rs = s.executeQuery();
            List<Review> reviews = new ArrayList<>();
            while (rs.next()) {
                reviews.add(new Review(rs.getInt(1), rs.getInt(2), rs.getString(3)));
            }
            return reviews;
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    public List<Book> all(){
        try {
            Statement stm = connection.createStatement();
            ResultSet rs = stm.executeQuery("select * from book");
            List<Book> books = new ArrayList<>();
            while (rs.next()) {
                books.add(new Book(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5)));
            }
            return books;
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }

    }

    public List<Book> findName(String name){
        try{
            String query = "SELECT * FROM book WHERE name = ?";
            PreparedStatement s = connection.prepareStatement(query);
            s.setString(1, name);
            ResultSet rs = s.executeQuery();
            List<Book> books = new ArrayList<>();
            while (rs.next()) {
                books.add(new Book(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5)));
            }
            return books;
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    public List<Book> findAuthor(String author){
        try{
            String query = "SELECT * FROM book WHERE author = ?";
            PreparedStatement s = connection.prepareStatement(query);
            s.setString(1, author);
            ResultSet rs = s.executeQuery();
            List<Book> books = new ArrayList<>();
            while (rs.next()) {
                books.add(new Book(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5)));
            }
            return books;
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    public List<Book> findGenra(String name){
        try{
            String query = "SELECT * FROM book WHERE genra = ?";
            PreparedStatement s = connection.prepareStatement(query);
            s.setString(1, name);
            ResultSet rs = s.executeQuery();
            List<Book> books = new ArrayList<>();
            while (rs.next()) {
                books.add(new Book(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5)));
            }
            return books;
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    public List<Book> findYear(int name){
        try{
            String query = "SELECT * FROM book WHERE year = ?";
            PreparedStatement s = connection.prepareStatement(query);
            s.setInt(1, name);
            ResultSet rs = s.executeQuery();
            List<Book> books = new ArrayList<>();
            while (rs.next()) {
                books.add(new Book(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5)));
            }
            return books;
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static void main(String[] args) {
        /*try {
            inst.add("Horten Hears a who", "person", "fiction", 2002);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }*/
        //System.out.println(inst.all());
        System.out.println(inst.findName("Horten Hears a who"));
    }

}
