package fit.biepjv.bookself.model;

public class Review {
    private int id, book_id;
    private String review;

    Review(int id, int book_id, String review){
        this.id = id;
        this.book_id = book_id;
        this.review = review;
    }

    public int getId() {
        return id;
    }

    public int getBook_id() {
        return book_id;
    }

    public String getReview() {
        return review;
    }
}
