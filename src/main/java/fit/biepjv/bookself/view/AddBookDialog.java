package fit.biepjv.bookself.view;

import com.sun.javafx.scene.control.IntegerField;
import fit.biepjv.bookself.model.BookDAO;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

import java.awt.*;
import java.awt.Label;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class AddBookDialog extends Dialog<ButtonType> {

    private final TextField nameTF, authorTF, YearIF;
    private final ChoiceBox genraType;
    private final BookPane bp;
    private final Alert alert;

    AddBookDialog(BookPane bpt){
        alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Input Error");
        alert.setContentText("Please fill in all the information and choose book genra." +
                "Please fill in the correct Year");
        bp = bpt;
        this.setTitle("Add New Book");
        this.setHeaderText("Please fill out your new book info below:");
        genraType = new ChoiceBox(FXCollections.observableArrayList("Fiction", "Non-Fiction", "Mystory", "Romance", "Sci-Fi", "Children", "Biography"));
        genraType.setMaxWidth(200);
        nameTF = new TextField("Enter Book Name");
        authorTF = new TextField("Enter Book Author");
        YearIF = new TextField("Year of Publication");
        Text lbName = new Text("Book Name: ");
        Text lbAuthor = new Text("Author: ");
        Text lbGenra = new Text("Genra: ");
        Text lbYear = new Text("Publication Year: ");

        GridPane grid = new GridPane();
        grid.addRow(0, lbName, nameTF);
        grid.addRow(1, lbAuthor, authorTF);
        grid.addRow(2, lbGenra, genraType);
        grid.addRow(3, lbYear, YearIF);
        grid.setHgap(15);
        grid.setVgap(15);
        grid.setPadding(new Insets(10,10,10,10));

        this.getDialogPane().setContent(grid);
        ButtonType buttonTypeOk = new ButtonType("Okay", ButtonBar.ButtonData.OK_DONE);
        this.getDialogPane().getButtonTypes().add(buttonTypeOk);

    }

    public void execute(){
        Optional<ButtonType> res= this.showAndWait();
        if(res.isEmpty() || res.get().equals(ButtonType.CANCEL)) return;
        try {
            int d = Integer.parseInt(YearIF.getText());
            BookDAO.inst.add(nameTF.getText(), authorTF.getText(), (String) genraType.getValue(), d);
        } catch (NumberFormatException nfe) {
            alert.showAndWait();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        bp.notifyBP();
    }

}
