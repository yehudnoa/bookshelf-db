package fit.biepjv.bookself.view;

import fit.biepjv.bookself.model.Book;
import fit.biepjv.bookself.model.BookDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.util.List;
import java.util.Optional;


public class BookPane extends TableView<Book> {

    private final static ObservableList<Book> books = FXCollections.observableArrayList();

    BookPane() {
        TableColumn<Book, Integer> colId = new TableColumn("ID");
        TableColumn<Book, String> colName = new TableColumn("Name");
        TableColumn<Book, String> colAuthor = new TableColumn("Author");
        TableColumn<Book, String> colGenra = new TableColumn("Genra");
        TableColumn<Book, Integer> colYear = new TableColumn("Year");
        colId.setCellValueFactory(new PropertyValueFactory<>("id"));
        colName.setCellValueFactory(new PropertyValueFactory<>("name"));
        colAuthor.setCellValueFactory(new PropertyValueFactory<>("author"));
        colGenra.setCellValueFactory(new PropertyValueFactory<>("genra"));
        colYear.setCellValueFactory(new PropertyValueFactory<>("year"));
        this.getColumns().addAll(colId, colName, colAuthor, colGenra, colYear);
        setItems(books);
    }

    public static void notifyBP(){
        books.setAll(BookDAO.inst.all());
    }

    public static void BPSelected(List<Book> b) { books.setAll(b);}

    public Optional<Book> getSelected(){
        return Optional.ofNullable(getSelectionModel().getSelectedItem());
    }
}
