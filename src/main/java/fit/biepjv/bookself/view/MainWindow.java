package fit.biepjv.bookself.view;

import fit.biepjv.bookself.model.Book;
import fit.biepjv.bookself.model.BookDAO;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.control.TextField;


import java.util.List;
import java.util.Optional;

public class MainWindow extends Application {

    private TextField tf1, tf2, tf3;
    private ChoiceBox cb;
    private GridPane grid;
    private Alert alert;
    private Background background;

    public static void main(String[] args)  {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        backgroundset();
        stage.setTitle("BookShelf");
        BookPane bp = new BookPane();
        bp.notifyBP();
        setGrid(bp);
        Button add = new Button("Add Book to Bookshelf");
        add.setOnAction(ae -> new AddBookDialog(bp).execute());
        Button delete = new Button("Delete Book");
        delete.setOnAction(ae -> {
            Optional<Book> books = bp.getSelected();
            books.ifPresent(id -> BookDAO.inst.delete(id.getId()));
            bp.notifyBP();
        });
        Button review = new Button("Book Review");
        review.setOnAction(ae -> {
            Optional<Book> books = bp.getSelected();
            books.ifPresent(id -> new ReviewDialog(id.getId(), id.getName()).execute());
        });
        add.setMaxWidth(200);
        delete.setMaxWidth(200);
        review.setMaxWidth(200);
        VBox mp = new VBox(grid, add, delete, review);
        mp.setSpacing(25);
        HBox bb = new HBox(mp, bp);
        bb.setBackground(background);
        bb.setSpacing(15);
        bb.setPadding(new Insets(20, 20, 20, 20));
        stage.setScene(new Scene(bb));
        stage.show();
        stage.centerOnScreen();

    }

    private void setGrid(BookPane bp) {
        Text lbName = new Text("Name: ");
        Text lbAuthor = new Text("Author: ");
        Text lbGenra = new Text("Genra: ");
        Text lbYear = new Text("Year: ");
        tf1 = new TextField();
        tf2 = new TextField();
        tf3 = new TextField();
        cb = new ChoiceBox(FXCollections.observableArrayList("Fiction", "Non-Fiction", "Mystory", "Romance", "Sci-Fi", "Children", "Biography"));
        grid = new GridPane();
        cb.setMaxWidth(200);
        Button findName = new Button("Find by Name");
        Button findAuthor = new Button("Find by Author");
        Button findGenra = new Button("Find by Genra");
        Button findYear = new Button("Find by Year");
        Button reset = new Button("Reset");
        findName.setMaxWidth(100);
        findAuthor.setMaxWidth(100);
        findGenra.setMaxWidth(100);
        findYear.setMaxWidth(100);
        reset.setMaxWidth(200);
        findName.setOnAction(actionEvent -> {
            if(tf1.getText().isEmpty()){
                alert.showAndWait();
            } else {
                bp.BPSelected(BookDAO.inst.findName(tf1.getText()));
            } });
        findAuthor.setOnAction(actionEvent -> {
            if(tf2.getText().isEmpty()){
                alert.showAndWait();
            } else {
                bp.BPSelected(BookDAO.inst.findAuthor(tf2.getText()));
            } });
        findGenra.setOnAction(actionEvent -> {
            if(tf1.getText().isEmpty()){
                alert.showAndWait();
            } else {
            bp.BPSelected(BookDAO.inst.findGenra((String) cb.getValue()));
            } });
        findYear.setOnAction(actionEvent -> {
            if(tf3.getText().isEmpty() || !validate(tf3.getText())){
                alert.showAndWait();
            } else {
                bp.BPSelected(BookDAO.inst.findYear(Integer.parseInt(tf3.getText())));
            }});
        reset.setOnAction(actionEvent -> {bp.notifyBP();});
        grid.addRow(0, lbName, tf1, findName);
        grid.addRow(1, lbAuthor,tf2, findAuthor);
        grid.addRow(2, lbGenra, cb, findGenra);
        grid.addRow(3, lbYear, tf3, findYear);
        grid.add(reset, 1, 4);
        grid.setHgap(7);
        grid.setVgap(7);
    }

    private void backgroundset(){
        BackgroundFill background_fill = new BackgroundFill(Color.STEELBLUE,
                CornerRadii.EMPTY, Insets.EMPTY);
        background = new Background(background_fill);
        alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Input Error");
        alert.setContentText("No information has been filled.");
    }

    private boolean validate(String text)
    {
        return text.matches("[0-9]*");
    }
}