package fit.biepjv.bookself.view;

import fit.biepjv.bookself.model.BookDAO;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

import java.util.Optional;

public class ReviewDialog extends Dialog<ButtonType> {

    private final TextField reviewTF;
    private final Alert alert;
    private final Background background;

    ReviewDialog(int book_id, String book_name){
        BackgroundFill background_fill = new BackgroundFill(Color.STEELBLUE,
                CornerRadii.EMPTY, Insets.EMPTY);
        background = new Background(background_fill);
        this.setTitle(book_name + " reviews");
        alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Input Error");
        alert.setContentText("Your review is empty.");
        reviewTF = new TextField();
        reviewTF.setMaxWidth(300);
        reviewTF.setMaxHeight(500);
        Text lbReview = new Text("Your Review: ");
        Button add = new Button("Add New Review");
        ReviewPane rp = new ReviewPane();
        rp.notifyRP(book_id);
        add.setOnAction(actionEvent -> {
            if(reviewTF.getText().isEmpty()){
                alert.showAndWait();
            } else {
                BookDAO.inst.addReview(book_id, reviewTF.getText());
                rp.notifyRP(book_id);
            }
        });
        HBox hb = new HBox(lbReview, reviewTF);
        hb.setPadding(new Insets(50, 5, 5, 5));
        hb.setSpacing(15);
        HBox hb2 = new HBox(hb, rp);
        hb2.setPadding(new Insets(10, 10, 10, 10));
        hb2.setSpacing(15);
        VBox vb = new VBox(hb2, add);
        hb.setSpacing(15);
        vb.setBackground(background);
        this.getDialogPane().setContent(vb);
        ButtonType buttonTypeOk = new ButtonType("Okay", ButtonBar.ButtonData.OK_DONE);
        this.getDialogPane().getButtonTypes().add(buttonTypeOk);
    }

    public void execute(){
        Optional<ButtonType> res= this.showAndWait();
    }

}
