package fit.biepjv.bookself.view;

import fit.biepjv.bookself.model.Book;
import fit.biepjv.bookself.model.BookDAO;
import fit.biepjv.bookself.model.Review;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class ReviewPane extends TableView<Review> {
    private final static ObservableList<Review> reviews = FXCollections.observableArrayList();

    ReviewPane(){

        TableColumn<Review, String> colReview = new TableColumn("REVIEW");
        colReview.setCellValueFactory(new PropertyValueFactory<>("Review"));
        this.getColumns().addAll(colReview);
        setItems(reviews);
    }

    public static void notifyRP(int book_id) { reviews.setAll(BookDAO.inst.returnReview(book_id));}

}
